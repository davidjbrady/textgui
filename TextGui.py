# Test commit from David Jay's new mac

from tkinter import *
import datetime

class Application(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self.master = master
        self.text = None
        self.countValue = 0
        self.createWidgets()
        
    def clearText(self):
       self.text.delete(1.0, END)
        
    def createWidgets(self):
        """Creates a simple GUI with a counter. """
        self.master.title("Text Editor")
        self.frameOne = Frame(self.master)
        self.frameOne.grid(row = 0, column = 0)
        self.countlbl = Label(self.frameOne, text = "Count: 0", background = "white", foreground = "blue", font = "Times 20", relief = "groove")
        self.countlbl.grid(row = 0, column = 0)
        self.incrbutton = Button(self.frameOne, text = "Increment", command = self.increment_count)
        self.incrbutton.grid(row = 1, column = 0)
        self.quitbutton = Button(self.frameOne, text = "Quit", command = self.master.destroy)
        self.quitbutton.grid(row = 1, column = 1)

        self.frametwo = Frame(self.master)
        self.frametwo.grid(row = 1, column = 0)
        self.clearbutton = Button(self.frametwo, text = "Clear", command = self.clearText)
        self.clearbutton.grid(row = 2, column = 0)
        self.text = Text(self.frametwo, wrap = NONE)
        self.text.grid(row = 0, column = 0)
        self.text.insert(1.0, "Enter text here")
        scroll_y = Scrollbar(self.frametwo, orient = VERTICAL, command = self.text.yview)
        self.text.config(yscrollcommand = scroll_y.set)
        scroll_y.grid(row = 0, column = 1, sticky = 'ns')
        scroll_x = Scrollbar(self.frametwo, orient = HORIZONTAL, command = self.text.xview)
        self.text.config(xscrollcommand = scroll_x.set)
        scroll_x.grid(row = 1, column = 0, sticky = "ew")
          
    
    def do_nothing(self):
        pass
    
    def increment_count(self):
        self.countValue += 1
        self.countlbl.configure(text='Count: ' + str(self.countValue))

if __name__ == "__main__":
    root = Tk()
    app = Application(root)
    root.mainloop()
